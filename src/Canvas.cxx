#include "Canvas.h"

#include "TPad.h"
#include "TCanvas.h"
#include "TPolyLine.h"
#include "TH2F.h"
#include "TString.h"
#include "TGraph.h"
#include "TMath.h"
#include "TLatex.h"
#include "TLegend.h"

#include <map>
#include <iostream>

//______________________________________________________________________________
//
Canvas::Canvas( const TString &name, const int x, const int y ):
m_name(name),
m_canvas(0),
m_x(-1),
m_y(-1),
m_isT(false),
m_singletDoubletDrawn(false)
{
  m_x = x;
  m_y = y;
}

//______________________________________________________________________________
//
Canvas::Canvas( const Canvas &q ):
m_name(q.m_name),
m_canvas(q.m_canvas),
m_x(q.m_x),
m_y(q.m_y),
m_isT(q.m_isT)
{
}

//______________________________________________________________________________
//
Canvas::~Canvas(){
  delete m_canvas;
}

//______________________________________________________________________________
//
void Canvas::AddPlot( int x, int y, const std::string &legend ){
  PlotDef plot;
  plot.x = x;
  plot.y = y;
  plot.legend = legend;
  plot.mass = atoi(legend.c_str());
  m_plots.push_back(plot);
}

//______________________________________________________________________________
//
void Canvas::Initialize(){

  //
  // Declares the ROOT TCanvas
  //
  m_canvas = new TCanvas((TString)("c_mult"+m_name),(TString)("c_mult"+m_name),900,800);

  //
  // Divide in pads by hand to set properly the margins:
  //
  const float xoff = 0.08;
  const float yoff = 0.08;
  const float xmax = 0.98;
  const float ymax = 0.98;
  float xwdt = (xmax- xoff)/m_x;
  float ywdt = (ymax - yoff)/m_y;

  //
  // Compute the angle of the diagonal
  //
  float angle = TMath::RadToDeg()*TMath::ATan2((float)m_x,(float)m_y);

  //
  // Forbidden area
  //
  float x_forb[4]  = {0., 1., 1., 0.};
  float y_forb[4]  = {1., 1., 0., 1.};
  float x_forb2[5] = {0., 1.02, 1.02, 1.0, 0.};
  float y_forb2[5]  = {1., 1., 0., 0., 1.};
  float x_forb3[4]  = {-0.05, 1., 1., -0.05};
  float y_forb3[4]  = {1.05, 1.05, 0., 1.05};

  TPolyLine *pForb = new TPolyLine(4,x_forb,y_forb);
  pForb->SetFillColor(kGray+1);
  pForb->SetFillStyle(3004);
  pForb->SetLineWidth(0);
  TPolyLine *pForb2 = new TPolyLine(5,x_forb2,y_forb2);
  pForb2->SetFillColor(kGray+1);
  pForb2->SetFillStyle(3004);
  pForb2->SetLineWidth(0);
  TPolyLine *pForb3 = new TPolyLine(4,x_forb3,y_forb3);
  pForb3->SetFillColor(kGray+1);
  pForb3->SetFillStyle(3004);
  pForb3->SetLineWidth(0);

  float x_forb_neg[4] = {-0.05, 0, 0, -0.05};
  float y_forb_neg[4] = {0.,0, 1.05, 1.05};

  TPolyLine *pForb_neg = new TPolyLine(4,x_forb_neg,y_forb_neg);
  pForb_neg->SetFillColor(kGray+1);
  pForb_neg->SetFillStyle(3004);
  pForb_neg->SetLineWidth(0);

  //
  // The font will have different size in the different pads since
  // the different pads have different sizes ... Need to adjust (using
  // pixel-based size)
  //
  const double font_factor = 0.08;
  double pad_width  = 0.;
  double pad_height = 0.;
  int charheight = 0;

  float x1(0),y1(0),x2(0),y2(0);
  int counter = 0;

  for( unsigned int iline = 0; iline < m_y; iline++ ) {
    if( iline != 0 ){
      y1 = yoff+ywdt*iline;
      y2 = y1+ywdt;
    } else {
      y1 = 0;
      y2 = ywdt+yoff;
    }

    for( unsigned int icolumn = 0; icolumn < m_x; icolumn++ ) {
      //Coordinates of the pads
      if( icolumn != 0 ){
        x1 = x2;
        x2 = x1+xwdt;
      } else {
        x1 = 0;
        x2 = xwdt+xoff;
      }

      //
      // Defines the name of the pad based on the legend or/skips the pad if blank is specified
      //
      std::string name = "";
      int mass = 0;
      for( const auto signal : m_plots ){
        if( signal.y != (m_y-iline-1) || signal.x != icolumn ) continue;
        name = signal.legend;
        mass = signal.mass;
      }
      if( mass == 0 && name=="" ){
        continue;
      }
      if( name=="blank" || name=="BLANK" ){
        continue;
      }

      //
      // Defining the Pad
      //
      TPad *cPada = new TPad( ("p"+std::to_string(mass)).c_str()+(TString)(m_name),
      ("p"+std::to_string(mass)).c_str(),
      x1,y1,x2,y2,0,0,0);
      cPada -> SetLeftMargin(0.0);
      cPada -> SetBottomMargin(0.0);

      int current_pad_width  = cPada->XtoPixel(gPad->GetX2());
      int current_pad_height = cPada->YtoPixel(gPad->GetY1());
      if(counter==0){
        pad_width  = current_pad_width;
        pad_height = current_pad_height;
      }

      if(icolumn==0){
        cPada -> SetLeftMargin(xoff/(xwdt+xoff));
      } else {
        cPada -> SetLeftMargin(0.0);
      }
      if(iline==0){
        cPada -> SetBottomMargin(yoff/(ywdt+yoff));
      } else {
        cPada -> SetBottomMargin(0.0);
      }
      cPada -> SetRightMargin(0.0);
      cPada -> SetTopMargin(0.0);
      cPada -> SetFillColor(0);
      cPada -> Draw();
      cPada -> cd();

      //
      // Defining a dummy histogram to fill in the pads
      //
      TH2F* new_VLQ_dummy = 0;
      if( iline==0 && icolumn==0 ){
        new_VLQ_dummy = new TH2F( ("new_VLQ_dummy"+std::to_string(mass)).c_str()+(TString)(m_name),
                                  ("new_VLQ_dummy"+std::to_string((iline+1)*(icolumn+1))).c_str(),
                                  10, -0.05, 0.9999, 10, 0., 0.9999);
      } else if( icolumn == m_x-1 ){
        new_VLQ_dummy = new TH2F( ("new_VLQ_dummy"+std::to_string(mass)).c_str()+(TString)(m_name),
                                  ("new_VLQ_dummy"+std::to_string((iline+1)*(icolumn+1))).c_str(),
                                  10, -0.05, 1.02, 10, 0., 0.9999);
      } else if( iline == m_y-1 &&  icolumn == m_x-1 ){
        new_VLQ_dummy = new TH2F( ("new_VLQ_dummy"+std::to_string(mass)).c_str()+(TString)(m_name),
                                  ("new_VLQ_dummy"+std::to_string((iline+1)*(icolumn+1))).c_str(),
                                  10, -0.05, 0.9999, 10, 0.0001, 1.05);
      } else {
        new_VLQ_dummy = new TH2F( ("new_VLQ_dummy"+std::to_string(mass)).c_str()+(TString)(m_name),
                                  ("new_VLQ_dummy"+std::to_string((iline+1)*(icolumn+1))).c_str(),
                                  10, -0.05, 0.9999, 10, 0.0001, 0.9999);
      }
      new_VLQ_dummy -> GetXaxis() -> SetNdivisions(406);
      new_VLQ_dummy -> GetYaxis() -> SetNdivisions(406);
      new_VLQ_dummy -> SetTitle("");

      if(iline!=0){
        new_VLQ_dummy -> GetXaxis() -> SetLabelSize(0.0);
      } else {
        new_VLQ_dummy -> GetXaxis() -> SetLabelFont( 43 );
        new_VLQ_dummy -> GetXaxis() -> SetLabelSize( 24 );
      }
      if(icolumn!=0){
        new_VLQ_dummy -> GetYaxis() -> SetLabelSize(0.0);
      } else {
        new_VLQ_dummy -> GetYaxis() -> SetLabelFont( 43 );
        new_VLQ_dummy -> GetYaxis() -> SetLabelSize( 22 );
      }
      new_VLQ_dummy -> Draw();

      //
      // Drawing the forbiden area
      //
      if ( icolumn == m_x-1 ){
        pForb2->SetFillColor(18);
        pForb2->SetFillStyle(1001);
        pForb2->SetLineWidth(0);
        pForb2->Draw("f");
      }
      else if ( iline == m_y-1 ){
        pForb3->SetFillColor(18);
        pForb3->SetFillStyle(1001);
        pForb3->SetLineWidth(0);
        pForb3->Draw("f");
      }
      else{
        pForb->SetFillColor(18);
        pForb->SetFillStyle(1001);
        pForb->SetLineWidth(0);
        pForb->Draw("f");
      }
      pForb_neg->SetFillColor(18);
      pForb_neg->SetFillStyle(1001);
      pForb_neg->SetLineWidth(0);
      pForb_neg->Draw("f");

      cPada->SetTickx();
      cPada->SetTicky();
      cPada->RedrawAxis();

      //
      // Drawing the "Unphysical" mention in the plot
      //
      TLatex* tforb2 = new TLatex(0.43,0.64,"Unphysical");
      tforb2->SetTextAlign(11);
      tforb2->SetTextAngle(-angle);
      tforb2->SetTextColor(kBlack);
      if (pad_width < pad_height){
        charheight = font_factor*pad_width;
      } else {
        charheight = font_factor*pad_height;
      }
      tforb2->SetTextFont(53);
      tforb2->SetTextSizePixels(charheight);
      tforb2->Draw("same");

      //
      // Adding the mass legend in each pad
      //
      TLatex* tln = new TLatex( name.size()>=4 ? 0.48-0.04:0.48,0.85,("m_{"+(std::string)(m_isT?"T":"B")+"} = "+name+" GeV").c_str());
      tln->SetTextAlign(11);
      tln->SetTextFont(63);
      tln->SetTextSize(20);
      tln->Draw();

      m_canvas->cd();
      counter++;

      m_pads.push_back(cPada);

    }
  }

  //
  // Adding the top-left "1"
  //
  m_canvas->cd();
  TLatex* t1 = new TLatex(0.074,0.99,"1");
  t1->SetNDC();
  t1->SetTextAlign(33);
  t1->SetTextColor(kBlack);
  t1->SetTextFont(43);
  t1->SetTextSize(22);
  t1->Draw();

  //
  // Adding the axes definition
  //
  TString xTitle = (m_isT) ? "BR(T #rightarrow Wb)" : "BR(B #rightarrow Wt)";
  TLatex* titleX = new TLatex( 0.80,0.01,xTitle);
  titleX->SetTextAlign(11);
  titleX->SetTextFont(43);
  titleX->SetTextSize(30);
  titleX->Draw();
  TString yTitle = (m_isT) ? "BR(T #rightarrow Ht)" : "BR(B #rightarrow Hb)";
  TLatex* titleY = new TLatex( 0.03,0.80,yTitle);
  titleY->SetTextAlign(11);
  titleY->SetTextFont(43);
  titleY->SetTextAngle(90);
  titleY->SetTextSize(30);
  titleY->Draw();

}

//______________________________________________________________________________
//
void Canvas::AddATLASLabel( const std::string &label, const double x, const double y ){
  m_canvas->cd();

  TLatex* atlasTex2 = new TLatex(x,y,"ATLAS");
  atlasTex2->SetNDC();
  atlasTex2->SetTextFont(72);
  atlasTex2->SetTextSize(0.035);
  atlasTex2->Draw();

  TLatex* atlasTex2p = new TLatex(x+0.11,y,label.c_str());
  atlasTex2p->SetNDC();
  atlasTex2p->SetTextFont(42);
  atlasTex2p->SetTextSize(0.033);
  atlasTex2p->Draw();
}

//______________________________________________________________________________
//
void Canvas::AddECMLumiLabel( const std::string &label, const double x, const double y ){
  m_canvas->cd();

  TLatex *eneTex2 = new TLatex();
  eneTex2->SetTextSize(20);
  eneTex2->SetTextFont(43);
  eneTex2->SetNDC(1);
  eneTex2->DrawLatex(x, y, label.c_str());
}

//______________________________________________________________________________
//
void Canvas::AddAnalysisLabel( const std::string &label, const double x, const double y ){
  m_canvas->cd();

  TLatex *eneTex2 = new TLatex();
  eneTex2->SetTextSize(20);
  eneTex2->SetTextFont(43);
  eneTex2->SetNDC(1);
  eneTex2->DrawLatex(x, y, label.c_str());
}

//______________________________________________________________________________
//
TPad* Canvas::GetPad( std::string &name ){
  for( auto *pad : m_pads ){
    if((TString)pad->GetName()==("p"+name).c_str()+(TString)(m_name)){
      return pad;
    }
  }
  return 0;
}

//______________________________________________________________________________
//
void Canvas::DrawSingletDoublet(){
  for( auto signal : m_plots ){
    double x_doublet = 0;
    double x_singlet = 0;
    double y_doublet = 0;
    double y_singlet = 0;
    if( signal.mass == 600 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.49380;
        y_doublet = 0.61539;
        y_singlet = 0.31151;
      }
    } else if( signal.mass == 700 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.49340;
        y_doublet = 0.58716;
        y_singlet = 0.29746;
      }
    } else if( signal.mass == 750 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.49347;
        y_doublet = 0.57640;
        y_singlet = 0.29196;
      }
    } else if( signal.mass == 800 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.49362;
        y_doublet = 0.56730;
        y_singlet = 0.28727;
      }
    } else if( signal.mass == 850 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.49378;
        y_doublet = 0.55956;
        y_singlet = 0.28727;
      }
    } else if( signal.mass == 900 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.495;
        y_doublet = 0.555;
        y_singlet = 0.280;
      }
    } else if( signal.mass == 950 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.495;
        y_doublet = 0.549;
        y_singlet = 0.277;
      }
    } else if( signal.mass == 1000 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.495;
        y_doublet = 0.544;
        y_singlet = 0.275;
      }
    } else if( signal.mass == 1050 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.4953;
        y_doublet = 0.540;
        y_singlet = 0.2724;
      }
    } else if( signal.mass == 1100 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.4953;
        y_doublet = 0.536;
        y_singlet = 0.2704;
      }
    } else if( signal.mass == 1150 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.496;
        y_doublet = 0.533;
        y_singlet = 0.269;
      }
    } else if( signal.mass == 1200 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.496;
        y_doublet = 0.530;
        y_singlet = 0.267;
      }
    } else if( signal.mass == 1300 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.496;
        y_doublet = 0.525;
        y_singlet = 0.265;
      }
    } else if( signal.mass == 1400 ){
      if(m_isT){
        x_doublet = 0.000;
        x_singlet = 0.496;
        y_doublet = 0.521;
        y_singlet = 0.262;
      }
    }

    for( auto *temp_pad : m_pads ){
      if((TString)temp_pad->GetName()==("p"+signal.legend).c_str()+(TString)(m_name)){
        temp_pad -> cd();
      }
    }

    TGraph* gDoublet = new TGraph(1);
    gDoublet -> SetPoint(0, x_doublet, y_doublet );
    gDoublet -> SetMarkerStyle(29);
    gDoublet -> SetMarkerSize(1.7);
    gDoublet -> Draw("p");

    TGraph* gSinglet = new TGraph(1);
    gSinglet -> SetPoint(0, x_singlet, y_singlet );
    gSinglet -> SetMarkerStyle(20);
    gSinglet -> SetMarkerSize(1.5);
    gSinglet -> Draw("p");
  }
  m_singletDoubletDrawn = true;
}

//______________________________________________________________________________
//
void Canvas::AddLegendItem( const int index, const TString &legend, TH2F* expected, TH2F* observed ){
  Legend leg;
  leg.expected = expected;
  leg.observed = observed;
  leg.legend = legend;
  m_legends.insert( std::pair < int, Legend>( index, leg));
}

//______________________________________________________________________________
//
void Canvas::DrawLegend( const double x1, const double y1, const double x2, const double y2 ){
  //
  // There are some different legends to draw
  //
  //  1 -> The legend for the singlet/doublet scenarios
  //  2 -> The legend for each analysis (color)
  //  3 -> The legend for each plot type (expected, observed)
  //
  //  2 and 3 are merged in case there is only one analysis
  //
  int n_entries = 1;// doublet/singlet on one single line
  if(m_legends.size()>1){
    n_entries += 1;//the general histogram legend
    n_entries += m_legends.size();//one line by legend
  } else {
    n_entries += m_legends.size()*2;//two lines by analysis
  }
  double y_step = fabs(y2-y1)/n_entries;

  m_canvas -> cd();

  if(m_singletDoubletDrawn){
    TGraph* gDoublet = new TGraph(1);
    gDoublet -> SetMarkerStyle(29);
    gDoublet -> SetMarkerSize(1.7);

    TGraph* gSinglet = new TGraph(1);
    gSinglet -> SetMarkerStyle(20);
    gSinglet -> SetMarkerSize(1.5);

    TLegend *leg = new TLegend(x1, y1, x2, y1+y_step);
    leg -> SetLineColor(0);
    leg -> SetFillStyle(0);
    leg -> SetNColumns(2);
    leg -> AddEntry(gDoublet,"SU(2) doublet","p");
    leg -> AddEntry(gSinglet,"SU(2) singlet","p");
    leg -> Draw();
  }

  if(m_legends.size()>1){
    TLegend *leg2 = new TLegend(x1, y2-y_step, x2, y2);
    leg2 -> SetLineColor(0);
    leg2 -> SetFillStyle(0);
    leg2 -> SetNColumns(2);
    TH1F *exp = new TH1F();
    exp -> SetLineWidth(3);
    exp -> SetLineStyle(2);
    exp -> SetLineColor(kBlack);
    TH1F *obs = new TH1F();
    obs -> SetLineWidth(2);
    obs -> SetLineStyle(1);
    obs -> SetLineColor(kBlack);
    obs -> SetFillColorAlpha(kBlack, 0.35);
    leg2 -> AddEntry(exp,"Exp. limit","l");
    leg2 -> AddEntry(obs,"Obs. limit","f");
    leg2 -> Draw();

    TLegend *leg3 = new TLegend(x1, y1+y_step, x2, y2-y_step);
    leg3 -> SetLineColor(0);
    leg3 -> SetFillStyle(0);
    for( auto analysis : m_legends ){
      TH1F *ana = new TH1F();
      ana -> SetLineWidth(2);
      ana -> SetLineStyle(1);
      ana -> SetLineColor( analysis.second.expected -> GetLineColor() );
      leg3 -> AddEntry(ana,analysis.second.legend,"l");
    }
    leg3 -> Draw();
  } else if ( m_legends.size() == 1 ){
    TLegend *leg2 = new TLegend(x1, y1+y_step, x2, y2-y_step);
    leg2 -> SetLineColor(0);
    leg2 -> SetFillStyle(0);

    if(m_legends.begin() -> second.expected){
      TH1F *exp = new TH1F();
      exp -> SetLineWidth(3);
      exp -> SetLineStyle(2);
      exp -> SetLineColor( m_legends.begin() -> second.expected -> GetLineColor() );
      leg2 -> AddEntry(exp,"95% CL expected limit","l");
    }
    if(m_legends.begin() -> second.observed){
      TH1F *obs = new TH1F();
      obs -> SetLineWidth(2);
      obs -> SetLineStyle(1);
      obs -> SetLineColor( m_legends.begin() -> second.observed -> GetLineColor());
      obs -> SetFillColorAlpha( m_legends.begin() -> second.observed -> GetLineColor(), 0.35);
      leg2 -> AddEntry(obs,"95% CL observed limit","f");
    }
    leg2 -> Draw();
  }
}

//______________________________________________________________________________
//
void Canvas::Print( const std::string &output, const std::vector < std::string > &formats ){
  for ( const auto format : formats ){
    m_canvas->Print( (output+format).c_str() );
  }
}
