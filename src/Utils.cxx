#include "Utils.h"

#include "TH2F.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TRandom3.h"
#include "TStyle.h"

#include <iostream>
#include <fstream>

//______________________________________________________________________________
//
Utils::Utils( const TString &name ):
m_name(name),
m_inputTextFile(""),
m_prefix(""),
m_mass_template(""),
m_br_steps(0.05),
m_BR_template(""),
m_expected(""),
m_observed(""),
m_dumpControl(false),
m_outputFolder("")
{
  m_combinations.clear();
}

//______________________________________________________________________________
//
Utils::Utils( const Utils &q ):
m_name(q.m_name),
m_inputTextFile(q.m_inputTextFile),
m_prefix(q.m_prefix),
m_mass_template(q.m_mass_template),
m_br_steps(q.m_br_steps),
m_BR_template(q.m_BR_template),
m_expected(q.m_expected),
m_observed(q.m_observed),
m_dumpControl(q.m_dumpControl),
m_combinations(q.m_combinations),
m_outputFolder(q.m_outputFolder)
{}

//______________________________________________________________________________
//
Utils::~Utils()
{
  m_combinations.clear();
}
