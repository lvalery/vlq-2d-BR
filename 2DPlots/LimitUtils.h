#ifndef LIMITUTILS_H
#define LIMITUTILS_H

class TH2F;
class TPad;

#include <string>
#include <sstream>
#include <vector>

#include "Utils.h"

class LimitUtils : public Utils {

public:

  //____________________________________________________________________________
  // Standard C++ functions
  LimitUtils( const TString &name );
  LimitUtils( const LimitUtils & );
  ~LimitUtils();

  //____________________________________________________________________________
  // Specific functions
  void Build2DPlot( const TString &name, TH2F* &observed, TH2F* &expected ) ;
  void AddResults( TPad* pad, TH2F* exp, TH2F* obs );

  //____________________________________________________________________________
  // Specific functions
  inline void SetContourLevel( const double level ){ m_contourLevel = level; };
  inline void SetColor( const int color ){ m_plot_color = color; };

private:
  double m_contourLevel;
  int m_plot_color;

};

#endif //LIMITUTILS_H
