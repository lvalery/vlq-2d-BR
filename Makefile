SDIR  = ./src/
IDIR  = ./2DPlots/
LDIR  = ./bin/
UDIR  = ./util/

MAKEFLAGS = --no-print-directory -r -s
INCLUDE = $(shell root-config --cflags)
LIBS    = $(shell root-config --libs)

BINS = limit
BINS2 = significance
OBJS =

all: $(BINS)

$(BINS): % :  $(UDIR)Limit.cxx
	@echo "Building $@ ... "
	mkdir -p $(LDIR)
	$(CXX) $(CCFLAGS) $< -I$(IDIR) $(INCLUDE) $(LIBS) $(SDIR)Canvas.cxx $(SDIR)Utils.cxx $(SDIR)LimitUtils.cxx -o "$(LDIR)Limit.exe"
	@echo "Done"

clean:
	rm -rf $(LDIR)
