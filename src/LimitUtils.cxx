#include "LimitUtils.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TStyle.h"
#include <iostream>
#include <fstream>

//______________________________________________________________________________
//
LimitUtils::LimitUtils( const TString &name ):
Utils(name),
m_contourLevel(0.99),
m_plot_color(kBlack)
{}

//______________________________________________________________________________
//
LimitUtils::LimitUtils( const LimitUtils &q ):
Utils(q),
m_contourLevel(q.m_contourLevel),
m_plot_color(q.m_plot_color)
{}

//______________________________________________________________________________
//
LimitUtils::~LimitUtils()
{}

//______________________________________________________________________________
//
void LimitUtils::Build2DPlot( const TString &name, TH2F* &th2_expected, TH2F* &th2_observed ) {

  std::cout << "-> Playing with mass: " << name << std::endl;

  //
  // Bulding a vector of combinations
  //
  for( double iWb = 0. ; iWb <= 1.+0.001; iWb += m_br_steps ){
    for( double iHt = 0. ; iHt  <= 1.+0.001; iHt += m_br_steps ){
      for( double iZt = 0. ; iZt  <= 1.+0.001; iZt += m_br_steps ){
        if( fabs( (iWb+iZt+iHt) - 1. ) > 1e-04 ){
          continue;
        }
        TString br_template = m_BR_template;
        br_template = br_template.ReplaceAll("BRWB",Form("%.2f",iWb));
        br_template = br_template.ReplaceAll("BRHT",Form("%.2f",iHt));
        br_template = br_template.ReplaceAll("BRZT",Form("%.2f",iZt));
        Combination combi;
        combi.legend      = name;
        combi.description = br_template;
        combi.mass        = atoi(name);
        combi.Wb          = iWb;
        combi.Ht          = iHt;
        combi.Zt          = iZt;
        m_combinations.push_back(combi);
      }
    }
  }

  //
  // Define the ranges
  //
  int nPoints = 1./m_br_steps + 1;
  float maxBin = 1.0+m_br_steps/2.;
  float minBin = 0.0-m_br_steps/2.;

  //
  // TH2F creation
  //
  th2_observed = new TH2F("observed_" + name + m_name, "", nPoints+1,
                          minBin-m_br_steps, maxBin,
                          nPoints+1, minBin-m_br_steps, maxBin);
  th2_expected = new TH2F("expected_" + name + m_name, "", nPoints+1,
                          minBin-m_br_steps, maxBin,
                          nPoints+1, minBin-m_br_steps, maxBin);

  //
  // Open the text file
  //
  std::string line;
  std::ifstream myfile (m_inputTextFile);
  if (myfile.is_open())
  {
    bool known_signal = false;
    Combination current_combination;
    while ( getline (myfile,line) )
    {
      TString ts_line = (TString)line;
      if( ts_line.Contains(m_prefix) ){
        for( const auto combi : m_combinations ){
          TString mass_template = m_mass_template;
          mass_template = mass_template.ReplaceAll("MASS",name);
          if( ts_line.Contains(mass_template) && ts_line.Contains(combi.description) ){
            current_combination = combi;
            known_signal = true;
            break;
          } else {
            known_signal = false;
          }
        }
        continue;
      }
      if( !known_signal ) continue;

      //
      // Now takes the good values
      //
      if(ts_line.Contains(m_observed)){
        ts_line.ReplaceAll(m_observed,"");
        double observed = atof(ts_line);
        th2_observed -> SetBinContent( th2_observed->FindBin(current_combination.Wb,current_combination.Ht), (observed>=1 ? 0.1 : 1) );
      }
      if(ts_line.Contains(m_expected)){
        ts_line.ReplaceAll(m_expected,"");
        double expected = atof(ts_line);
        th2_expected -> SetBinContent( th2_expected->FindBin(current_combination.Wb,current_combination.Ht), (expected>=1 ? 0.1 : 1) );
      }
    }
    myfile.close();
  }

  if(m_dumpControl){
    for ( const auto hist : {th2_expected,th2_observed} ){
      //Canvas
      TCanvas c;
      TString canvas_name = (TString)hist->GetName();
      c.SetName(canvas_name);
      hist -> Draw("colztext");
      gStyle -> SetOptStat(0);
      //Printing the output
      TString outputName = m_outputFolder;
      outputName += "/Limits/Control/";
      gSystem -> mkdir(outputName,true);
      outputName += canvas_name;
      outputName += ".png";
      c.Print(outputName);
    }
  }
}

//______________________________________________________________________________
//
void LimitUtils::AddResults( TPad* pad, TH2F* exp, TH2F* obs ){

  //
  // Getting the pad and drawing the contours
  //
  pad -> cd();

  //
  // Set the colors
  //
  if(exp)exp -> SetLineColor(m_plot_color);
  if(obs)obs -> SetLineColor(m_plot_color);

  if(obs){
    //
    // Observed contour
    //
    TH2D *clon_obs = (TH2D*)obs->Clone("expclon");
    //contour
    clon_obs -> SetContour(1);
    clon_obs -> SetContourLevel(0,m_contourLevel);
    //line
    clon_obs -> SetLineWidth(4);
    clon_obs -> SetLineStyle(2);
    clon_obs -> SetLineColor(m_plot_color);
    //fill
    clon_obs -> SetFillStyle(0);
    clon_obs -> SetFillColorAlpha(m_plot_color, 0.35);
    //drawing a filled area
    clon_obs -> Draw("CONTsame");
  }

  if(exp){
    //
    // Expected contour
    //
    TH2D *clon = (TH2D*)exp->Clone("expclon");
    //contour
    clon -> SetContour(1);
    clon -> SetContourLevel(0,m_contourLevel);
    //line
    clon -> SetLineWidth(3);
    clon -> SetLineStyle(2);
    clon -> SetLineColor(m_plot_color);
    //drawing a line
    clon -> Draw("CONT3same");
  }

}
