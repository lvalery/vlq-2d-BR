#ifndef CANVAS_H
#define CANVAS_H

class TCanvas;
class TPad;
class TH2F;

#include "TString.h"

#include <vector>
#include <map>
#include <string>

class Canvas {

public:

  //____________________________________________________________________________
  // STRUCTS
  struct PlotDef{
    int x;
    int y;
    int mass;
    std::string legend;
  };
  struct Legend {
    TH2F* expected;
    TH2F* observed;
    TString legend;
  };

  //____________________________________________________________________________
  // General C++ functions
  Canvas( const TString &name, const int x = 3, const int y = 4 );
  Canvas( const Canvas &q );
  ~Canvas();

  //____________________________________________________________________________
  // Specific functions
  void AddPlot(  int x, int y, const std::string &legend );
  void Initialize();
  void AddATLASLabel( const std::string &label, const double x = 0.71, const double y = 0.94 );
  void AddECMLumiLabel( const std::string &label, const double x = 0.71, const double y = 0.90);
  void AddAnalysisLabel( const std::string &label, const double x = 0.71, const double y = 0.86);
  TPad* GetPad( std::string &name );
  void DrawSingletDoublet();
  void AddLegendItem( const int index, const TString &legend, TH2F* expected, TH2F* observed );
  void DrawLegend( const double x1, const double x2, const double y1, const double y2 );
  void Print( const std::string &output, const std::vector < std::string > &formats );

  //____________________________________________________________________________
  // Specific functions
  inline void IsT() { m_isT = true; }
  inline std::vector < PlotDef > GetPlots() { return m_plots; }

private:
  TString m_name;
  TCanvas *m_canvas;
  int m_x;
  int m_y;
  std::vector < PlotDef > m_plots;
  std::vector < TPad* > m_pads;
  bool m_isT;
  bool m_singletDoubletDrawn;
  std::map < int, Legend > m_legends;
};

#endif //CANVAS_H
