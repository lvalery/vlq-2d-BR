#include "TColor.h"

#include "Canvas.h"
#include "LimitUtils.h"

int main(int argc, char const *argv[]) {

  //////////////////////////////////////////////////////////////////////////////
  //
  //
  //    DECLARING THE CANVAS
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  // Instantiation of the canvas
  //
  Canvas *c1l = new Canvas("1l",3,4);
  c1l -> IsT();

  Canvas *c0l = new Canvas("0l",3,4);
  c0l -> IsT();

  Canvas *c0l1l = new Canvas("0l1l",3,4);
  c0l1l -> IsT();

  Canvas *cCombi = new Canvas("Combi",3,4);
  cCombi -> IsT();

  Canvas *cAll = new Canvas("cAll",3,4);
  cAll -> IsT();

  //
  // Defining the signal mass points to consider
  // c -> AddPlot(x,y,"mass");
  //        --> with x and y the coordinates (x=0 and y=0 corresponds to the top left corner)
  //        --> mass is the mass in GeV
  //
  for ( auto *c : {c1l,c0l,c0l1l,cCombi,cAll} ){
    c -> AddPlot(0,0,"600");
    c -> AddPlot(1,0,"700");
    c -> AddPlot(2,0,"blank");
    c -> AddPlot(0,1,"750");
    c -> AddPlot(1,1,"800");
    c -> AddPlot(2,1,"850");
    c -> AddPlot(0,2,"900");
    c -> AddPlot(1,2,"950");
    c -> AddPlot(2,2,"1000");
    c -> AddPlot(0,3,"1050");
    c -> AddPlot(1,3,"1100");
    c -> AddPlot(2,3,"1150");
    c -> Initialize();

    //
    // Defining some labels
    //
    c -> AddATLASLabel("Internal", 0.7, 0.94);
    c -> AddECMLumiLabel("#sqrt{s} = 13 TeV, 13.2  fb^{-1}", 0.7, 0.90);
  }

  //
  // Adding labels for the analysis type (when needed)
  //
  c1l -> AddAnalysisLabel("Ht+X 1-lep", 0.7, 0.86);
  c0l -> AddAnalysisLabel("Ht+X 0-lep", 0.7, 0.86);
  cCombi -> AddAnalysisLabel("Ht+X Combination", 0.7, 0.86);


  //////////////////////////////////////////////////////////////////////////////
  //
  //
  //    DECLARING THE LIMITUTILS OBJECTS
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //
  // Looping of the masses to consider to build the 2D plots (control plots also dumped)
  //
  LimitUtils *limits_1l = new LimitUtils("1l");
  LimitUtils *limits_0l = new LimitUtils("0l");
  LimitUtils *limits_Combi = new LimitUtils("Combi");

  //which text file is to be used
  limits_1l -> SetInputTextFile("test_results_limits_1L.txt");
  limits_0l -> SetInputTextFile("test_results_limits_0L.txt");
  limits_Combi -> SetInputTextFile("test_results_limits_Combi.txt");
  //defines the color for this analysis
  limits_1l -> SetColor( kBlue+1  );
  limits_0l -> SetColor( kGreen+2 );
  limits_Combi -> SetColor( kRed+1 );
  //Produce control plots
  limits_1l -> DumpControlPlots( "Plots1lep/" );
  limits_0l -> DumpControlPlots( "Plots0lep/" );
  limits_Combi -> DumpControlPlots( "PlotsCombi/" );

  //
  // Defining the common criteria
  //
  for( auto *limit : {limits_1l,limits_0l,limits_Combi} ){

    //parametrize the contour plots
    limit -> SetContourLevel(0.99);

    //how to detect the presence of the mass point
    //for instance: VLQ_TT_600_Wb0.00_Ht0.00_Zt1.00_.txt
    //-- Prefix: to identify the lines containing the limits
    limit -> Prefix("VLQ_TT");
    //-- How to recognize the mass of the VLQ
    limit -> Mass("_MASS_");
    //-- How to recognize the BR configuration
    limit -> SetBRScanSteps(0.05);
    limit -> BRTemplate("BRWB_BRZT_BRHT");//BRWB, BRHT, BRZT will be replaced by the good values

    //--how to identify expected limits
    limit -> ExpectedIs("Expected:");
    limit -> ObservedIs("Observed:");
  }

  for( auto signal : c1l -> GetPlots() ){
    if(signal.legend=="blank") continue;
    TH2F* expected = 0;
    TH2F* observed = 0;

    for( auto limit : {limits_0l,limits_1l,limits_Combi} ){
      limit -> Build2DPlot( signal.legend, expected, observed );

      if(limit == limits_0l){
        TPad* pad = c0l -> GetPad(signal.legend);
        limit -> AddResults( pad, expected, observed );
        c0l -> AddLegendItem(1, "Ht+X 0-lep", expected, observed);

        pad = c0l1l -> GetPad(signal.legend);
        limit -> AddResults( pad, expected, observed );
        c0l1l -> AddLegendItem(1, "Ht+X 0-lep", expected, observed);

        pad = cAll -> GetPad(signal.legend);
        limit -> AddResults( pad, expected, 0 );
        cAll -> AddLegendItem(1, "Ht+X 0-lep", expected, 0);
      }

      if(limit == limits_1l){
        TPad* pad = c1l -> GetPad(signal.legend);
        limit -> AddResults( pad, expected, observed );
        c1l -> AddLegendItem(0, "Ht+X 1-lep", expected, observed);

        pad = c0l1l -> GetPad(signal.legend);
        limit -> AddResults( pad, expected, observed );
        c0l1l -> AddLegendItem(0, "Ht+X 1-lep", expected, observed);

        pad = cAll -> GetPad(signal.legend);
        limit -> AddResults( pad, expected, 0 );
        cAll -> AddLegendItem(0, "Ht+X 1-lep", expected, 0);
      }


      if(limit == limits_Combi){
        TPad* pad = cCombi -> GetPad(signal.legend);
        limit -> AddResults( pad, expected, observed );
        cCombi -> AddLegendItem(2, "Ht+X Combination", expected, observed);

        pad = cAll -> GetPad(signal.legend);
        limit -> AddResults( pad, expected, observed );
        cAll -> AddLegendItem(2, "Ht+X Combination", expected, observed);

      }
    }
  }

  //
  // Printing the results
  //
  c1l -> DrawSingletDoublet();
  c1l -> DrawLegend(0.7,0.76,0.97,0.89);
  c1l -> Print("Limit_1l",{".eps",".png",".pdf"});

  c0l -> DrawSingletDoublet();
  c0l -> DrawLegend(0.7,0.76,0.97,0.89);
  c0l -> Print("Limit_0l",{".eps",".png",".pdf"});

  c0l1l -> DrawSingletDoublet();
  c0l1l -> DrawLegend(0.7,0.76,0.97,0.89);
  c0l1l -> Print("Limit_0l1l",{".eps",".png",".pdf"});

  cCombi -> DrawSingletDoublet();
  cCombi -> DrawLegend(0.7,0.76,0.97,0.89);
  cCombi -> Print("Limit_Combi",{".eps",".png",".pdf"});

  cAll -> DrawSingletDoublet();
  cAll -> DrawLegend(0.7,0.76,0.97,0.89);
  cAll -> Print("Limit_CombiAll",{".eps",".png",".pdf"});

  return 0;
}
