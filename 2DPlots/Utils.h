#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <sstream>
#include <vector>

#include "TString.h"
class TH2F;

class Utils {

public:

  //
  // Enum for get each combination
  //
  struct Combination {
    TString legend;
    TString description;
    int mass;
    double Wb;
    double Ht;
    double Zt;
  };

  //____________________________________________________________________________
  // Standard C++ functions
  Utils( const TString &name );
  Utils( const Utils & );
  ~Utils();

  //____________________________________________________________________________
  // Inline functions
  inline void SetInputTextFile( const TString &name ){ m_inputTextFile = name; };
  inline void Prefix( const TString &name ){ m_prefix = name; };
  inline void Mass( const TString &name ){ m_mass_template = name; };
  inline void SetBRScanSteps( const double br) { m_br_steps = br; };
  inline void BRTemplate( const TString &name ){ m_BR_template = name; };
  inline void ExpectedIs( const TString &name ){ m_expected = name; };
  inline void ObservedIs( const TString &name ){ m_observed = name; };
  inline void DumpControlPlots( const TString &name ) { m_outputFolder = name; m_dumpControl = true; };

protected:
  TString m_name;
  TString m_inputTextFile;
  TString m_prefix;
  TString m_mass_template;
  double m_br_steps;
  TString m_BR_template;
  TString m_expected;
  TString m_observed;
  bool m_dumpControl;
  std::vector < Combination > m_combinations;
  TString m_outputFolder;

};
#endif //UTILS_H
